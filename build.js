import path from 'path'
import { find, template } from 'lodash'
import component from 'Components/index'

// select the current route object from the routes array
const getRoute = (routes, path) => 
  find(
    routes,
    r => (r.url == path))

// get the current view string
const getView = (routes, path) => getRoute(routes, path).view

// given the current path, get the corresponding content
const getContent = (path) => require(`./src/content${(path == '/') ? '/home' : path}`)

// process a view with lodash.template, pass in a data object
const main = data => (
  template(require(`./src/views${getView(data.routes,data.path)}.html`))({
    ...data,
    component,
    route: getRoute(data.routes,data.path),
    content: getContent(data.path)
  })
)

// export the markup (async)
export default data => Promise.resolve(`
<!DOCTYPE html>
<html>
  <head><title>${data.site.title}</title></head>
  <body>

    ${component.Nav(data.routes, data.path)}

    ${main(data)}

    <script src="/client.bundle.js"></script>
  </body>
</html>
`)
