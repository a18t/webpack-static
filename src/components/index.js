import Nav from './Nav'
import Content from './Content'

export default {
  Nav,
  Content
}
