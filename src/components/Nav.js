export default (routes, path) => (
  routes
    .map(({title, url}) => `<a href="${url}" style="background: ${path == url? '#ddd' : '#fff'}">${title}</a>`)
    .join(' | ')
)


