# A static site generator using webpack.

[![Netlify Status](https://api.netlify.com/api/v1/badges/3270d5c0-8511-4bd6-bb7c-262b3826a4ea/deploy-status)](https://app.netlify.com/sites/a18t-webpack-static/deploys)

[Live Demo](https://a18t-webpack-static.netlify.com/)

Route data is stored in ```src/data/routes.json```.

Views (html processed with lodash.template) are stored in 	```src/views/```.

Components (js exporting a string) are stored in ```src/components/```.

Content (md) is stored in ```src/content/```.

A client-only bundle is built from entry point ```src/client.js```.

Run a build process with ```yarn build```.
