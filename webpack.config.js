const path = require('path')
const fs = require('fs')
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin')

// convert a relative path to a full path
const root = relativePath => path.resolve(__dirname, relativePath)

// get site data
const siteData = require(root('src/data/site.json'))

// get routes
const routeData = require(root('src/data/routes.json'))

// get paths array from the routes data
const getPaths = routes => routes.map(r => r.url)
const paths = getPaths(routeData)

// assign views to routes: choose view matching path, else default
const withViews = routes => (
  routes.map(
    r => {
      let slug = (r.url == "/") ? "/home" : r.url
      let view = root(`src/views${slug}.html`)
      return {
        ...r,
        view: !fs.existsSync(view)
          ? `/default`
          : slug
      }
    }))
const routes = withViews(routeData)

// this object gets passed to each page via StaticSiteGeneratorPlugin
const locals = {
  routes,
  site: siteData
}

module.exports = {

  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './build'
  },

  entry: {
    build: root('./build.js'),
    client: root('src/client.js')
  },

  output: {
    filename: '[name].bundle.js',
    path: root('build'),
    libraryTarget: 'umd'
  },

  module: {
    rules: [

      // js
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              [
                "@babel/preset-env"//,
                //{
                //"useBuiltIns": "entry"
                //}
              ]
            ]
          }
        }
      },

      // md
      {
        test: /\.md$/,
        use: [
          { loader: "html-loader" },
          { loader: "markdown-loader" }
        ]
      },

      // html
      {
        test: /\.html$/,
        use: [
          { loader: "html-loader" }
        ],
      }

    ]
  },

  resolve: {
    extensions: ['.js','.json','.md'],
    alias: {
      Content: root('src/content/'),
      Views: root('src/views/'),
      Components: root('src/components/'),
      Data: root('src/data/')
    }
  },

  plugins: [

    new StaticSiteGeneratorPlugin({
      entry: 'build',
      paths,
      locals,
      globals: {
        window: {}
      }
    })

  ]

}
